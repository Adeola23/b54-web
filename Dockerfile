FROM  node:14.17.6

WORKDIR /app

COPY dist/ssr /app/

RUN npm install -g

CMD ["npm", "start"]
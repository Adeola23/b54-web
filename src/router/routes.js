
const routes = [
  {
    path: "/",
    component: () => import("layouts/LoginRegisLayout.vue"),
    children: [
      { path: "", name:'Login', component: () => import("pages/users/Login.vue") },
      {
        path: "auth/register",
        name: "Signup",
        component: () => import("pages/users/Register.vue"),
      },
      { path: "/home", name:'Home', component: () => import("pages/home/HomePage.vue") },
      { path: "/cart", name:'Cart', component: () => import("pages/cart/Cart.vue") },
    ],
    
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/ErrorNotFound.vue"),
  },
];

export default routes
